FROM python:3.8-slim-buster
RUN apt-get update && apt-get install -y cron procps && rm -rf /var/lib/apt/lists/*
COPY dyndns-crontab /etc/cron.d/dyndns-crontab
RUN chmod 0644 /etc/cron.d/dyndns-crontab && crontab /etc/cron.d/dyndns-crontab
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY __main__.py __main__.py
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]





