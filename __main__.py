# This example requires the requests library be installed.  You can learn more
# about the Requests library here: http://docs.python-requests.org/en/latest/
from requests import get
import json
import ovh
import os
from datetime import datetime


def get_ip():
    try:
        ip = get('https://api.ipify.org').content.decode('utf8')
        print(f'{datetime.now()} My public IP address is: {ip}')
        return ip
    except Exception as ex:
        print(ex)
        raise Exception("Failed to get my external ip")

def ovh_authenticate(endpoint, app_key, app_secret, con_key):
    try:
        client = ovh.Client(
            endpoint=endpoint,               # Endpoint of API OVH Europe (List of available endpoints)
            application_key=app_key,    # Application Key
            application_secret=app_secret, # Application Secret
            consumer_key = con_key
        )
        return client
    except Exception as ex:
        print(ex)
        raise Exception("Unable to authenticate")

def get_domain_id(domain, subdomain, client):
    try:
        result = client.get(f'/domain/zone/{domain}/dynHost/record', subDomain=subdomain, )
        if isinstance(result, list) and len(result)==1:
            return result[0]
        else:
            print(f"{datetime.now()} Unable to get subdomain IP")
            raise Exception("Unable to get subdomain id")
    except Exception as ex:
        print(ex)
        raise Exception("Unable to get subdomain id")

def update_zone(client, domain, subdomain, id, ip):
    try: 
        result = client.put(f'/domain/zone/{domain}/dynHost/record/{id}',
            ip=ip, 
            subDomain=subdomain,
        )
        return True
    except Exception as ex:
        print(ex)
        raise Exception("Unable to update Zone")

def get_current_subdomain_ip(client, domain, subdomain, id):
    try: 
        result = client.get(f'/domain/zone/{domain}/dynHost/record/{id}')
        if isinstance(result, dict) and 'ip' in result:
            print (f"{datetime.now()} The current ip for {subdomain}.{domain} is {result['ip']}")
            return result['ip']
        else:
            print(f"{datetime.now()} Unable to get subdomain IP")
            raise Exception("Unable to get subdomain id")
    except Exception as ex:
        print(ex)
        raise Exception("Unable to update Zone")

def get_environment_data():
    try:
        endpoint = os.environ['OVH_ENDPOINT']
        app_key = os.environ['OVH_APP_KEY']
        app_secret = os.environ['OVH_APP_SECRET']
        cons_key = os.environ['OVH_CONS_KEY']
        domain = os.environ['OVH_TARGET_DOMAIN']
        subdomain = os.environ['OVH_TARGET_SUBDOMAIN']
        return endpoint, app_key, app_secret, cons_key, domain, subdomain
    except Exception as ex:
        print (ex)
        raise Exception("Unable to get the required environment variables")

endpoint, app_key, app_secret, cons_key, domain, subdomain = get_environment_data()
ip = get_ip()
client = ovh_authenticate(endpoint, app_key, app_secret, cons_key)
id = get_domain_id(domain, subdomain, client)
current_ip = get_current_subdomain_ip(client, domain, subdomain, id)
if current_ip == ip:
    print (f"{datetime.now()} No update required, closing")
else:
    print (f"{datetime.now()} Update required, updating the ip for {subdomain}.{domain} to {ip}")
    update_zone(client, domain, subdomain, id, ip)
    print (f"{datetime.now()} done")






