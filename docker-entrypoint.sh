#!/bin/sh
/usr/local/bin/python3 /app/__main__.py
printenv | sed 's/^\(.*\)$/export \1/g' | grep -E "^export OVH" > /project_env.sh
chmod +x /project_env.sh
cron -f
